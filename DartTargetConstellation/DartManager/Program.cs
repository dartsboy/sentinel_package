﻿using Constellation;
using Constellation.Package;
using System;
using DartLib;
using DartLib.Modes;
using DartLib.Modes.CutThroatCricket;
using System.Media;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using DartManager.Modes;

namespace DartManager
{
    public class Program : PackageBase,DartGameObserver
    {
        static void Main(string[] args)
        {
            PackageHost.Start<Program>(args);
        }
        private DartLib.DartGame game;

        public override void OnStart()
        {
            LinkedList<Player> players = new LinkedList<Player>();
            players.AddLast(new CutThroatCricketPlayer(1, "Thomas","hello"));
            game = new DartGame(players,"301");
            update();
            PackageHost.WriteInfo("Package starting - IsRunning: {0} - IsConnected: {1}", PackageHost.IsRunning, PackageHost.IsConnected);
        }

        private void update()
        {
            PackageHost.PushStateObject<DartGame>("DartGame", game);
        }

        /// <summary>
        /// Permet de démarrer une nouvelle partie
        /// </summary>
        /// <param name="game"> Paramétres de la partie à lancée </param>
        [MessageCallback]
        void initGame(GameObject game)
        {
            LinkedList<Player> players = new LinkedList<Player>();
            String typeGame = game.Type;
            foreach(PlayerObject p in game.Players)
            {
                players.AddLast(PlayerFactory.getPlayer((int)p.Id,(String)p.Name, typeGame,(string)p.Url));
            }
            this.game = new DartGame(players, typeGame);

            update();
            
        }
        /// <summary>
        /// Permet de passer au joueur suivant
        /// </summary>
        [MessageCallback]
        void next()
        {
            game.next();
            update();
        }
        /// <summary>
        /// Permet d'annuler la derniére fléchette
        /// </summary>
        [MessageCallback]
        void cancel()
        {
            if(game.undoShoot())
                update();
        }

        /// <summary>
        /// Indique un tire sur la cible
        /// </summary>
        /// <param name="multiple"> Multiple de la flechette </param>
        /// <param name="value"> Valeur de la flechette </param>
        [MessageCallback]
        void onDartTargetTouch(short multiple,short value)
        {
            PackageHost.WriteInfo(multiple + " "+ value);
            if(game.applyShoot(new Shoot(multiple, value), this))
            {

            }
              update();
            
        }

        public void SendEvent(DartEvent dartEvent)
        {

            PackageHost.SendMessage(MessageScope.Create(MessageScope.ScopeType.Group,"DartsDashboard"), dartEvent.Method, dartEvent.Param);
        }
    }
}
