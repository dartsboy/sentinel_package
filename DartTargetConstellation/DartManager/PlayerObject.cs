﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DartManager
{
    /// <summary>
    /// Objet décrivant un joueur qui souhaite rejoindre une partie
    /// </summary>
    class PlayerObject
    {
        /// <summary>
        /// ID du joueur
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nom du joueur
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Url de l'image du joueur
        /// </summary>
        public String Url { get; set; }
    }
}
