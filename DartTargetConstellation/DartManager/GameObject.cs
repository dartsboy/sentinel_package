﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DartManager
{
    /// <summary>
    /// Objet décrivant une partie a lancée
    /// </summary>
    class GameObject
    {
        /// <summary>
        /// Type de game (ex. "301", "Cricket Cut Throat", ...)
        /// </summary>
        public String Type { get; set; }

        /// <summary>
        /// Liste des joueurs
        /// </summary>
        public LinkedList<PlayerObject> Players { get; set; }
    }
}
