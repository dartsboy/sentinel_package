﻿using System;
using System.Collections.Generic;
using System.Linq;
using Constellation.Package;
using System.Text;
using System.Threading.Tasks;
using DartManager;

namespace DartLib
{
    [StateObject]
    public class DartGame
    {
               
        public LinkedList<Player> Players { get; set; }
        public String Type { get; set; }
        public short CurrentRound { get; set; }
        public short NbRound { get; set; }
        public Player CurrentPlayer { get; set; }

        private LinkedList<Player> winners;
        public LinkedList<Player> Winners {
            get{ return winners;}
        }
        private int currentPLayerindex;
        public DartGame()
        {
                
        }
        public DartGame(LinkedList<Player> Players, String Type)
        {
            this.Type = Type;
            this.Players = Players;
            CurrentRound = 1;
            winners = new LinkedList<Player>() ;
            currentPLayerindex = 0;
            CurrentPlayer = Players.ElementAt<Player>(0);
            CurrentPlayer.newRound();
           
        }
        public bool applyShoot(Shoot shoot, DartGameObserver dartGameObserver)
        {
            if (winners.Count != 0)
                return false;

            if (CurrentPlayer != null)
            {
                CurrentPlayer.newShoot(shoot, Players, dartGameObserver);
                winners = CurrentPlayer.getWinner(this);
                return true;
            }
            return true;
        }

        public bool undoShoot()
        {

            if (winners.Count != 0)
                return false;
            if (!CurrentPlayer.undoShoot(Players) && !(CurrentRound == 1 && currentPLayerindex == 0))
            {
                if (updatePrevPlayer())
                {
                    CurrentRound--;
                }
            }
            return true;
        }


        public bool next()
        {

            CurrentPlayer.closeROund();
            winners = CurrentPlayer.getWinner(this);
                if (this.CurrentRound >= CurrentPlayer.NbRound && this.Players.Last.Value == CurrentPlayer )
                    return false;
            if (updateNextPlayer())
            {
                CurrentRound++;
            }
            CurrentPlayer.newRound();
            return true;
        }
        public bool updateNextPlayer()
        {
            bool updateRound = false;
            int nb = Players.Count();
            currentPLayerindex++;
            if (currentPLayerindex == nb)
            {
                currentPLayerindex = 0;
                updateRound = true;
            }
            CurrentPlayer = Players.ElementAt<Player>(currentPLayerindex);
            if (CurrentPlayer.IsSpoiled)
            {

                CurrentPlayer.newRound();
                CurrentPlayer.closeROund();
                return updateNextPlayer();
            }
            return updateRound;
        }
        public bool updatePrevPlayer()
        {
            bool updateRound = false;
            currentPLayerindex--;
            int nb = Players.Count();
            if (currentPLayerindex == -1)
            {
                currentPLayerindex = nb-1;
                updateRound = true;
            }
            CurrentPlayer = Players.ElementAt<Player>(currentPLayerindex);
            if (CurrentPlayer.IsSpoiled)
            {
                while(CurrentPlayer.undoShoot(Players)){

                }
                return updatePrevPlayer();
            }
            return updateRound;
        }
    }
}
