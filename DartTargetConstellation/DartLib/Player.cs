﻿using System;
using System.Collections.Generic;
using System.Linq;
using Constellation.Package;
using System.Text;
using System.Threading.Tasks;
using DartManager;

namespace DartLib
{
    [StateObject]
    public abstract class Player
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public int Score { get; set; }
        public bool IsSpoiled { get; set; }
        public int NbRound { get; set; }
        public LinkedList<Round> Rounds { get; }
        public String Url { get; set; }
        private Round currentRound;

        public Round CurentRound
        {
            get { return currentRound; }
        }

        public Player(int id, String name, int score,String url)
        {
            Id = id;
            Name = name;
            Url = url;
            Score = score;
            IsSpoiled = false;
            NbRound = 20;
            currentRound = null;
            Rounds = new LinkedList<Round>();
                
        }

        protected abstract void applyShoot(Shoot shoot, LinkedList<Player> Players, DartGameObserver dartGameObserver);
        protected abstract void cancelShoot(Shoot shoot, LinkedList<Player> Players);
        public abstract LinkedList<Player> getWinner(DartGame game);

        public bool newShoot(Shoot shoot, LinkedList<Player> Players, DartGameObserver dartGameObserver)
        {
            if (currentRound != null)
            {
                if (currentRound.CanShoot())
                {
                    this.applyShoot(shoot, Players, dartGameObserver);
                    return currentRound.newShoot(shoot);
                }
            }
            return false;
        }
        public void newRound()
        {
            currentRound = new Round();
            Rounds.AddLast(currentRound);

        }
        public void closeROund()
        {
            if (currentRound != null)
            {
                currentRound.close();
            }
        }
        public bool undoShoot(LinkedList<Player> Players)
        {
            if(currentRound != null)
            {

            Shoot shoot = currentRound.LastShoot;

            if(currentRound.cancelShoot() && shoot != null)
            {
                cancelShoot(shoot,Players);
                return true;
            }
            if (Rounds.Count > 0)
            {

                Rounds.RemoveLast();
                if (Rounds.Count > 0)
                {
                    currentRound = Rounds.Last.Value;
                }
                else
                {
                    currentRound = null;
                }
            }

            }
            return false;

        }

        public virtual void addToScore(int value)
        {
            Score+=value;

        }
        public virtual void removeToScore(int value)
        {
            Score -= value;

        }
    }
}
