﻿using DartLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DartManager.Modes.Special
{
    public class BullOnlyPlayer : Player
    {
        int BULL_VALUE = 25;
        int MIN_SCORE = 0;

        public BullOnlyPlayer(int id, string name, string url) : base(id, name, 0, url) {
            NbRound = 10;
            base.Score = MIN_SCORE;
        }

        protected override void cancelShoot(Shoot shoot, LinkedList<Player> Players)
        {
            if(shoot.Value == BULL_VALUE)
            {
                int dart_point = this.getDartPoints(shoot);
                base.addToScore(-dart_point);
            }
        }

        public override LinkedList<Player> getWinner(DartGame game)
        {
            LinkedList<Player> players = new LinkedList<Player>();
            bool noMoreRounds = false;
            int current_winner_value = MIN_SCORE;
            if (game.CurrentRound >= this.NbRound && game.Players.Last.Value == this && !this.CurentRound.CanShoot())
            {
                noMoreRounds = true;
            }
            if (noMoreRounds)
            {
                foreach (var player in game.Players)
                {
                    if (player.Score > current_winner_value)
                    {
                        players = new LinkedList<Player>();
                        players.AddLast(player);
                        current_winner_value = player.Score;
                    }
                    else if ((player.Score == current_winner_value))
                    {
                        players.AddLast(player);
                    }

                }
            }
            return players;
        }

        protected override void applyShoot(Shoot shoot, LinkedList<Player> Players, DartGameObserver dartGameObserver)
        {

            if (shoot.Value == BULL_VALUE)
            {
                int dart_point = this.getDartPoints(shoot);
                base.addToScore(dart_point);
            }
        }

        public int getDartPoints(Shoot shoot)
        {
            return shoot.Type * shoot.Value;
        }

        
    }
}
