﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DartLib.Modes.CutThroatCricket
{
//garder les points mit et a qui
    
    public class Goals
    {
        const int GOAL_LIMIT = 3;
        public Dictionary<int, int> ListGoals { get; set; }

        public Goals()
        {
            ListGoals = new Dictionary<int, int>();
            ListGoals.Add(20, 0);
            ListGoals.Add(19, 0);
            ListGoals.Add(18, 0);
            ListGoals.Add(17, 0);
            ListGoals.Add(16, 0);
            ListGoals.Add(15, 0);
            ListGoals.Add(25, 0);
            
        }
        public bool isGoal(int value)
        {
            return ListGoals.ContainsKey(value);
        }
        public int applyShoot(Shoot shoot)
        {
            int value = shoot.Value;

            if (!ListGoals.ContainsKey(value))
            {
                return 0;
            }
            int multiple = shoot.Type;
            int currentMultiple = ListGoals[value];

            int returnedValue = value * Math.Max((multiple - Math.Max((GOAL_LIMIT - currentMultiple),0)),0);
            ListGoals[value] = currentMultiple + multiple;
            return returnedValue;
        }
        public int cancelShoot(Shoot shoot)
        {
            int value = shoot.Value;

            if (!ListGoals.ContainsKey(value))
            {
                return 0;
            }
                int multiple = shoot.Type;
            int currentMultiple = ListGoals[value];

            int pointGiven = Math.Max(( currentMultiple-GOAL_LIMIT),0);
            int returnedValue = - value * Math.Min(pointGiven,multiple);

            ListGoals[value] = currentMultiple - multiple;
            return returnedValue;
        }

        internal bool hasValidateAllGoals()
        {
            return hasValidateGoal(25) && hasValidateGoal(20) && hasValidateGoal(19) && hasValidateGoal(18) && hasValidateGoal(17) && hasValidateGoal(16) && hasValidateGoal(15);
        }

        public bool hasValidateGoal(int goal)
        {
            return ListGoals.ContainsKey(goal) && ListGoals[goal] >= GOAL_LIMIT;
        }
    }
}
