﻿using DartManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DartLib.Modes.CutThroatCricket
{
    public class CutThroatCricketPlayer : Player
    {
        public Goals ListGoals { get; set; }

        
        public CutThroatCricketPlayer(int id, string name,String url) : base(id, name, 0,url)
        {
            ListGoals = new Goals();
        }
        private void sendEventToObserver(Shoot shoot, LinkedList<Player> Players, DartGameObserver dartGameObserver)
        {
            if (dartGameObserver == null)
            {
                return;
            }
            int shootValue = shoot.Value;
            int shootMultiple = shoot.Type;
            if (this.ListGoals.isGoal(shootValue))
            {
                if (IsGoalClose(shootValue, Players))
                {
                    //SoBad
                    if(shootValue == 25 || shootMultiple > 1)
                    {
                        dartGameObserver.SendEvent(new DartEvent("SoBad", 0));
                    }
                }
                else
                {
                    switch (shootMultiple)
                    {
                        case (1):
                            if (shootValue == 25)
                            {
                                dartGameObserver.SendEvent(new DartEvent("Bull", 0));
                            }
                            else
                            {
                                dartGameObserver.SendEvent(new DartEvent("Good", 0));
                            }
                            break;

                        case (2):
                            if (shootValue == 25)
                            {

                                dartGameObserver.SendEvent(new DartEvent("DBull", 0));
                            }
                            else
                            {

                                dartGameObserver.SendEvent(new DartEvent("Double", 0));
                            }
                            
                            break;

                        case (3):

                            dartGameObserver.SendEvent(new DartEvent("Triple", 0));
                            break;
                    }
                }
            }
        }

        protected override void applyShoot(Shoot shoot, LinkedList<Player> Players, DartGameObserver dartGameObserver) 
        {
           
            int pointToApply = ListGoals.applyShoot(shoot);
            this.sendEventToObserver(shoot, Players, dartGameObserver);
            if (pointToApply >= 0)
            {

                foreach (CutThroatCricketPlayer player in Players)
                {
                    if (player != null && player != this )
                    {
                        if (!player.ListGoals.hasValidateGoal(shoot.Value))
                        {
                            player.addToScore(pointToApply);
                        }
                        if(!player.IsSpoiled)
                            player.checkIsSpoil(this);
                    }
                }
            }
        }
        private bool IsGoalClose(int value, LinkedList<Player> players)
        {
            foreach(CutThroatCricketPlayer p in players)
            {
                if (!p.ListGoals.hasValidateGoal(value))
                {
                    return false;
                }
            }
            return true;
        }
        private void sendEvent(Shoot shoot)
        {

        }

        private void checkIsSpoil(CutThroatCricketPlayer player)
        {
            IsSpoiled = player.Score <= this.Score && player.ListGoals.hasValidateAllGoals();
        }

        protected override void cancelShoot(Shoot shoot, LinkedList<Player> Players)
        {
            int pointToApply = ListGoals.cancelShoot(shoot);
            if (pointToApply <= 0)
            {

                foreach (Player player in Players)
                {
                    CutThroatCricketPlayer ctplayer = player as CutThroatCricketPlayer;
                    if (ctplayer != null && ctplayer != this )
                    {
                        if (!ctplayer.ListGoals.hasValidateGoal(shoot.Value))
                        {
                            ctplayer.addToScore(pointToApply);
                        }

                        if(ctplayer.IsSpoiled)
                            ctplayer.checkIsSpoil(this);
                    }
                }
            }
        }

        new public void addToScore(int value)
        {
                base.addToScore(value);

        }
        new public void removeToScore(int value)
        {
            if (!ListGoals.hasValidateGoal(value))
            {
                base.removeToScore(value);
            }

        }

        public override LinkedList<Player> getWinner(DartGame game)
        {
            LinkedList<Player> winners = new LinkedList<Player>();
           // int value = game.Players.First.Value.Score;
            int value = 999999;
            if (game.CurrentRound >= this.NbRound && game.Players.Last.Value == this && !this.CurentRound.CanShoot())
            {
                foreach(var p in game.Players)
                {
                    if (!p.IsSpoiled)
                    {


                        if (p.Score < value)
                        {
                            winners = new LinkedList<Player>();
                            winners.AddLast(p);
                        }
                        else if (p.Score == value)
                        {
                            winners.AddLast(p);
                        }
                    }
                }
            }
            else
            {
                CutThroatCricketPlayer win = null;
                    if (this.ListGoals.hasValidateAllGoals())
                    {
                    bool isGameWOn = true;
                        foreach(CutThroatCricketPlayer p2 in game.Players)
                        {
                            if(this != p2)
                            {
                                if ( !p2.IsSpoiled)
                                {
                                    isGameWOn = false;
                                    break ;
                                }

                            }
                        }
                        if(isGameWOn)
                        {
                            winners.AddLast(this);
                            return winners;
                        }

                    }
                
            }
            return winners;
        }
    }
    
}
