﻿using System;
using DartLib;
using DartLib.Modes.CutThroatCricket;
using DartLib.Modes;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DartManager.Modes.Special;

namespace DartManager.Modes
{
    public class PlayerFactory
    {
        public static Player getPlayer(int Id,String Name,String TypeOfGame,String url)
        {
            Player p = null;
            switch (TypeOfGame)
            {
                case ("Cricket Cut Throat"):
                        p = new CutThroatCricketPlayer(Id, Name,url);
                    break;
                case ("301"):
                    p = new _301Player(Id, Name,url);
                    break;
                case ("501"):
                    p = new _501Player(Id, Name,url);
                    break;
                case ("701"):
                    p = new _701Player(Id, Name,url);
                    break;
                case ("1001"):
                    p = new _1001Player(Id, Name,url);
                    break;
                case ("Bull Only"):
                    p = new BullOnlyPlayer(Id, Name, url);
                    break;
            }
            return p;

        }
    }
}
