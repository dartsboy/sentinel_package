﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DartLib.Modes
{
    public class _301Player : Games01
    {
        const int MAX_SCORE = 301;
        public _301Player(int id, string name, string url) : base(id, name,url)
        {
            NbRound = 10;
            base.Score = MAX_SCORE;
        }
    }
}
