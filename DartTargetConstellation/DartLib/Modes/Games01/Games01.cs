﻿using DartManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DartLib.Modes
{
    public abstract class Games01 : Player
    {
        const int SCORE_LIMIT = 0;
        const int MAX_SCORE = 150;
        const int MAX_DART = 3;
        const int GIANT_VALUE = 999999;
        public LinkedList<Shoot> BestShoots { get; set; }
        List<int> possible_shots;
        LinkedList<int> board_values;
        public Games01(int id, string name, string url) : base(id, name, 0, url)
        {
            this.InitializePossibleShotsList();
            this.BestShoots = new LinkedList<Shoot>();
        }

        public void InitializePossibleShotsList()
        {
            this.possible_shots = new List<int>();
            this.board_values = new LinkedList<int>();
            int sum = 0;
            for (int value = 1; value <= 20; value++)
            {
                this.board_values.AddFirst(value);
                for (int multiple = 1; multiple <= 3; multiple++)
                {
                    sum = value * multiple;
                    if (multiple > 1 && sum <= 20) { }
                    else
                    {
                        this.possible_shots.Add(value * multiple);
                    }

                }

            }
            this.possible_shots.Add(25);
            this.board_values.AddFirst(25);
            this.possible_shots.Add(50);
            this.board_values.AddFirst(50);
            this.possible_shots = this.possible_shots.OrderByDescending(a => a).ToList();
        }
        protected override void cancelShoot(Shoot shoot, LinkedList<Player> Players)
        {
            int dart_point = this.getDartPoints(shoot);
            addToScore(+dart_point);
            
        }

        protected override void applyShoot(Shoot shoot, LinkedList<Player> Players, DartGameObserver dartGameObserver)
        {


            int dart_point = this.getDartPoints(shoot);
            shoot_sounds(shoot, dartGameObserver);
            if (this.isOverTheLimit(dart_point, Score))
            {
                dartGameObserver.SendEvent(new DartEvent("Busted", 0));
                shoot.Type = -1;
                shoot.Value = -1;
                base.closeROund();

            }
            else
            {
                base.addToScore(-dart_point);
            }
            if (Score <= MAX_SCORE)
            {
                LinkedList<Shoot> shoots = new LinkedList<Shoot>();
                this.BestShoots = getShootsToWin(Score, this.dartLeft(), shoots);
            }
            
        }

        private int dartLeft()
        {
            var real_number_of_shoots = this.CurentRound.shoots.Count() + 1;
            if (this.CurentRound.shoots.Count() + 1 == MAX_DART)
            {
                return MAX_DART;
            }
            else
            {
                return MAX_DART - real_number_of_shoots;
            }
        }

        private void shoot_sounds(Shoot shoot, DartGameObserver dartGameObserver)
        {
            if(dartGameObserver == null)
            {
                return;
            }
            switch (shoot.Type)
            {
                case (1):
                    if (shoot.Value == 25)
                    {
                        dartGameObserver.SendEvent(new DartEvent("Bull", 0));
                    }
                    break;
                case (2):

                    if (shoot.Value == 25)
                    {
                        dartGameObserver.SendEvent(new DartEvent("DBull", 0));
                    }
                    else
                    {
                        dartGameObserver.SendEvent(new DartEvent("Double", 0));
                    }
                    break;
                case (3):
                    dartGameObserver.SendEvent(new DartEvent("Triple", 0));
                    break;
            }

        }
        public override LinkedList<Player> getWinner(DartGame game)
        {
            LinkedList<Player> players = new LinkedList<Player>();
            bool noMoreRounds = false;
            int current_winner_value = GIANT_VALUE;
            if (game.CurrentRound >= this.NbRound && game.Players.Last.Value == this && !this.CurentRound.CanShoot())
            {
                noMoreRounds = true;
            }
            if (noMoreRounds)
            {
                foreach (var player in game.Players)
                {
                    if (player.Score < current_winner_value)
                    {
                        players = new LinkedList<Player>();
                        players.AddLast(player);
                        current_winner_value = player.Score;
                    }
                    else if ((player.Score == current_winner_value))
                    {
                        players.AddLast(player);
                    }

                }
                return players;
            }


            if (this.Score == 0)
            {
                players.AddLast(this);
                
            }

            return players;
        }

        private LinkedList<Shoot> getShootsToWin(int score, int dart_left, LinkedList<Shoot> shoots_to_win)
        {
            KeyValuePair<int, int> closerValue = new KeyValuePair<int, int>(SCORE_LIMIT, GIANT_VALUE);
            bool skip = false;
            var lastValue = this.possible_shots.Last();
            foreach (int value in this.possible_shots)
            {
                if (value <= score)
                {
                    int substract = score - value;
                    if (substract == SCORE_LIMIT)
                    {
                        shoots_to_win.AddLast(getShootFromValue(value));
                        break;
                    }
                    else
                    {
                        if (value.Equals(lastValue) || (substract < closerValue.Value && substract >= 0))
                        {
                            closerValue = new KeyValuePair<int, int>(value, substract);
                        }
                        if (substract > closerValue.Value)
                        {
                            if ((dart_left == MAX_DART - 1) && (substract < MAX_SCORE / MAX_DART))
                            {
                                if (!this.board_values.Contains(substract))
                                {
                                    skip = true;
                                    closerValue = new KeyValuePair<int, int>(SCORE_LIMIT, GIANT_VALUE);
                                }
                            }
                            if (!skip)
                            {
                                shoots_to_win.AddLast(getShootFromValue(closerValue.Key));
                                if (dart_left > 1)
                                {

                                    shoots_to_win = getShootsToWin(closerValue.Value, dart_left - 1, shoots_to_win);
                                    break;
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            return shoots_to_win;
        }

        private Shoot getShootFromValue(int value)
        {
            int dart_value;
            if (this.board_values.Contains(value))
            {
                return new Shoot(1, value);
            }
            else
            {
                for (int multiple = 3; multiple >= 1; multiple--)
                {
                    if (value % multiple == 0)
                    {
                        dart_value = value / multiple;
                        if (this.board_values.Contains(dart_value))
                        {
                            return new Shoot(multiple, dart_value);
                        }
                    }

                }
            }
            return null;
        }

        public bool isOverTheLimit(int dart_point, int score)
        {
            return score - dart_point < SCORE_LIMIT;
        }

        public int getDartPoints(Shoot shoot)
        {
            return shoot.Type * shoot.Value;
        }




    }
}
