﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DartLib.Modes
{
    public class _701Player : Games01
    {
        const int MAX_SCORE = 701;
        public _701Player(int id, string name,string url) : base(id, name, url)
        {
            NbRound = 20;
            base.Score = MAX_SCORE;
        }
    }
}
