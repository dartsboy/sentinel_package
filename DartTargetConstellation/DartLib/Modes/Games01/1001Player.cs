﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DartLib.Modes
{
    public class _1001Player : Games01
    {
        const int MAX_SCORE = 1001;
        public _1001Player(int id, string name, string url) : base(id, name,url)
        {
            base.Score = MAX_SCORE;
        }
    }
}
