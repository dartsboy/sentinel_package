﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DartManager
{
    public class DartEvent
    {
        public String Method { get; set; }
        public Object Param { get; set; }

        public DartEvent(String method, Object param)
        {
            Method = method;
            Param = param;
        }
    }
}
