﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DartManager
{
    public interface DartGameObserver
    {
        void SendEvent(DartEvent dartEvent);
    }
}
