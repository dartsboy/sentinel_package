﻿using System;
using System.Collections.Generic;
using Constellation.Package;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DartLib
{
    [StateObject]
    public class Round
    {
        public const int NB_START_SHOOT = 3;
        public LinkedList<Shoot> shoots { get; set; }
        public Shoot LastShoot { get; set; }

        public Round()
        {
            shoots = new LinkedList<Shoot>();
            LastShoot = null;
        }

        public bool newShoot(Shoot shoot)
        {
            if(shoots.Count < NB_START_SHOOT)
            {
                shoots.AddLast(shoot);
                LastShoot = shoot;
                return true;
            }
            return false;
        }
        public bool cancelShoot()
        {
            if(LastShoot != null)
            {
                    shoots.RemoveLast();
                try
                {
                    LastShoot = shoots.Last();
                }
                catch
                {
                    LastShoot = null;
                }
                    return true;
            }
            return false;
        }
        public bool CanShoot()
        {
            return shoots.Count < NB_START_SHOOT;
        }
        public void close()
        {
            while(shoots.Count < NB_START_SHOOT)
            {
                LastShoot = new Shoot(0, 0);
                shoots.AddLast(LastShoot);
            }
        }

        public int getNbShootLeft()
        {
            return (NB_START_SHOOT - shoots.Count);
        }

    }
}
