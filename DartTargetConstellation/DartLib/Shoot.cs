﻿using System;
using System.Collections.Generic;
using Constellation.Package;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DartLib
{
    public class Shoot
    {
        public Shoot(int type,int value) 
        {
            Type = type;
            Value = value;
        }
        public int Type { get; set; }

        public int Value { get; set; }
    }
}
