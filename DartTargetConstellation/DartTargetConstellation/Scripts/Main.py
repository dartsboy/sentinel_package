﻿
import Constellation

from Tkinter import *
import RPi.GPIO as GPIO
import time

def OnExit():
    pass

PIN_TRIPPLE_1 = 7
PIN_TRIPPLE_0 = 36
PIN_DOUBLE_1 = 13
PIN_DOUBLE_0 = 33
PIN_SINGLE_1 = 16
PIN_SINGLE_0 = 31
PIN_BULL = 22

PIN_9_14_B = 18
PIN_20_16_1 = 38
PIN_12_11_B = 32
PIN_5_8_2 = 29
PIN_6_2_3 = 15
PIN_10_15_4 = 40
PIN_13_17_5 = 11
PIN_4_3_6 = 35
PIN_18_19_7 = 37
PIN_1_7_8 = 12

def dartTouch(pinin,pinout):
    
    outputDictionary = {}
    outputDictionary[PIN_TRIPPLE_1] = [3,1]
    outputDictionary[PIN_DOUBLE_1] = [2,1]
    outputDictionary[PIN_SINGLE_1] = [1,1]
    outputDictionary[PIN_BULL] = [0,2]
    outputDictionary[PIN_SINGLE_0] = [1,0]
    outputDictionary[PIN_DOUBLE_0] = [2,0]
    outputDictionary[PIN_TRIPPLE_0] = [3,0]

    inputDictionary = {}
    inputDictionary[PIN_9_14_B] = [9,14,50]
    inputDictionary[PIN_20_16_1] = [20,16,1]
    inputDictionary[PIN_12_11_B] = [12,11,25]
    inputDictionary[PIN_5_8_2] = [5,8,2]
    inputDictionary[PIN_6_2_3] = [6,2,3]
    inputDictionary[PIN_10_15_4] = [10,15,4]
    inputDictionary[PIN_13_17_5] = [13,17,5]
    inputDictionary[PIN_4_3_6] = [4,3,6]
    inputDictionary[PIN_18_19_7] = [18,19,7]
    inputDictionary[PIN_1_7_8] = [1,7,8]

    isButton = False
    value = inputDictionary[pinin][outputDictionary[pinout][1]]
    multiple = outputDictionary[pinout][0]
    if(multiple == 0):
        isButton = True
        multiple = 1
        if(value == 50):
            multiple = 2
            value = 25
            isButton = False
        if(value == 25):
            multiple = 1
            isButton = False
    if isButton :
        Constellation.SendMessage("DartManager", "ButtonPressed",[ value ])
    else:
        Constellation.SendMessage("DartManager", "onDartTargetTouch",[ multiple, value ])
    time.sleep(0.8)

def dart():



    outs = [PIN_TRIPPLE_1,PIN_DOUBLE_1,PIN_SINGLE_1,PIN_BULL,PIN_SINGLE_0,PIN_DOUBLE_0,PIN_TRIPPLE_0]
    ins = [PIN_13_17_5,PIN_1_7_8,PIN_6_2_3,PIN_9_14_B,PIN_5_8_2,PIN_12_11_B,PIN_4_3_6,PIN_18_19_7,PIN_20_16_1,PIN_10_15_4]

    GPIO.setmode(GPIO.BOARD)

    for pinin in ins:
            GPIO.setup(pinin,GPIO.IN)
    for pinout in outs:
            GPIO.setup(pinout,GPIO.OUT)

    while True:
            for pinout in outs:
                    GPIO.output(pinout,GPIO.HIGH)
                    for pinin in ins:
                            if(GPIO.input(pinin)):
                                    dartTouch(pinin,pinout)
                    GPIO.output(pinout,GPIO.LOW)


def OnStart():
    dart()



# Start without break ! You need to implement a "While true" even if your script stop !
# Constellation.StartAsync()
# while Constellation.IsRunning:
#     pass
#     time.sleep(1) 

# Start with embedded loop (the code after this method will newer call)
# Constellation.Start(); 

# Start by calling your custom method then embedded loop to keep your script running
Constellation.Start(OnStart);